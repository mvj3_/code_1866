//题目分析:
//排序后隔三取一。用sort排序后是从小到大的。从后往前取。

//题目网址:http://soj.me/1438

#include <iostream>
#include <algorithm>

using namespace std;

int prices[20001] = {0};

int main()
{

    int testCases;
    int num;
    int maxDiscount;
    cin >> testCases;
    while (testCases--) {
        maxDiscount = 0;

        cin >> num;
        for (int i = 0; i < num; i++) {
            cin >> prices[i];
        }

        sort(prices, prices + num);

        if (num > 2) {
            for (int j = num - 3; j >= 0; j = j - 3) {
                maxDiscount += prices[j];
            }
        }

        cout << maxDiscount << endl;
    } 
    
    return 0;
}                                 